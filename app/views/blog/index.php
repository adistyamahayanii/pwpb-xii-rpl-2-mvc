<div class="container mt-5">

    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash(); ?>
        </div>
    </div>

   <div class="row">
       <div class="col-lg-6">
       <button type="button" class="btn btn-primary tombolTambahData" data-bs-toggle="modal"data-bs-target="#formModal">
        Tambah Data Blog
        </button>
        <br></br>
           <h3>Blog</h3>
           <ul class="list-group">
           <?php foreach($data["blog"] as $blog) :?>
                <li class="list-group-item">
                    <?=$blog['judul']; ?>

                    <a href="<?=BASE_URL;?>/blog/detail/<?= $blog['id']?>"
                    class="btn btn-primary float-right">Detail</a>
                    
                    <a href="<?=BASE_URL;?>/blog/ubah/<?= $blog['id']?>"
                    class="btn btn-success float-right tampilModalUbah" 
                    data-bs-toggle="modal"data-bs-target="#formModal"
                    data-id="<?= $blog['id'];?>">Ubah</a>

                    <a href="<?=BASE_URL;?>/blog/hapus/<?= $blog['id']?>"
                    class="btn btn-danger float-right"
                    onclick="return confirm('Yakin Kak?');">Hapus</a>
                </li>
           <?php endforeach; ?>
           </ul>
       </div>
   </div>
</div>


<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel">Tambah Data Blog</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

    <div class="modal-body">
        <form action="<?= BASE_URL; ?>/blog/tambah" method="post">
        <input type="hidden" name="id" id="id">
            <div class="form-group">
                        <label for="penulis">Penulis</label>
                        <input type="text" class="form-control" id="penulis" name="penulis">
                    
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul">
                
                        <label for="tulisan">Tulisan</label>
                        <input type="text" class="form-control" id="tulisan" name="tulisan">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
                </form>  
            </div>
        </div>
    </div>
</div>