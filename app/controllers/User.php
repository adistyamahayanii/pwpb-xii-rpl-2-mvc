<?php
   class User extends Controller {

       public function index () 
       {
           $data["username"] = "User";
           $data["user"] = $this->model("User_model")->getAllUser();
           $this->view("templates/header", $data);
           $this->view("user/index", $data);
           $this->view("templates/footer");
       }
       public function detail($id) {
            $data["username"] = "Detail Username";
            $data["user"] = $this->model('User_model')->getUserById($id);
            $this->view("templates/header", $data);
            $this->view("user/detail", $data);
            $this->view("templates/footer");
       }

       public function tambah()
       {
           if( $this->model('User_model')->tambahDataUser($_POST) > 0 ) {
               Flasher::setFlash('berhasil', 'ditambahkan', 'success');
               header('Location: ' . BASE_URL . '/user');
               exit;
           } else {
               Flasher::setFlash('gagal', 'ditambahkan', 'danger');
               header('Location: ' . BASE_URL . '/user');
               exit;
           }
       }
   
       public function hapus($id)
       {
           if( $this->model('User_model')->hapusDataUser($id) > 0 ) {
               Flasher::setFlash('berhasil', 'dihapus', 'success');
               header('Location: ' . BASE_URL . '/user');
               exit;
           } else {
               Flasher::setFlash('gagal', 'dihapus', 'danger');
               header('Location: ' . BASE_URL . '/user');
               exit;
           }
       }

       public function getubah()
       {
           echo json_encode($this->model('User_model')->getUserById
           ($_POST['id']));
       }
   
       public function ubah()
       {
           if( $this->model('User_model')->ubahDataUser($_POST) > 0 ) 
           {
               Flasher::setFlash('berhasil', 'diubah', 'success');
               header('Location: ' . BASE_URL . '/user');
               exit;
           } else {
               Flasher::setFlash('gagal', 'diubah', 'danger');
               header('Location: ' . BASE_URL . '/user');
               exit;
           } 
       }

   }
?>