<?php
    //class App dipanggil oleh file index yang berada di folder public (Teknik Bootstraping)
    class App {

        //properti class App {default}
        protected $controller = 'Register';
        protected $method = 'index';
        protected $params = [];

        public function __construct() {
            // panggil url
            $url = $this->parseURL();
            $controller = $url[0] ?? $this->controller;
            // var_dump($url);
            
            // SetupController

            // cek controller apakah ada file home.php di folder controllers, jika ada ditimpa oleh controller yang baru
            if( file_exists('../app/controllers/' . $controller . '.php') ) {
                //menimpa controller baru
                $this->controller = $controller;
                // menghilangkan controller dari elemenn array
                unset($url[0]);
            }
    
            // panggil controller 
            require_once '../app/controllers/'. $this->controller.'.php';
            //class dinstansiasi supaya bisa memanggil method
            $this->controller = new $this->controller;

            // Setup Method

            // cek method -> kalau kosong pakai method default, kalau ada dicek lagi
            if (isset($url[1])) {
                //cek method, apakah ada atau tidak dalam controller
                if (method_exists($this->controller, $url[1])) {
                    // menimpa method baru
                    $this->method = $url[1];
                    unset($url[1]);
                }
            }

            // Setup Params
            if (!empty($url)) {
                $this->params = array_values($url);
            }

            // Jalankan Controller & Method, serta kirimkan Params jika ada
            call_user_func_array([$this->controller, $this->method], $this->params);
            }

            public function parseURL()
            {
                // jika ada url yang dikirimkan maka kita akan ambil isi url tersebut
                if( isset($_GET['url']) ) {
                    // rtrim -> untuk menghapus tanda slash(/) url diakhir
                    $url = rtrim($_GET['url'], '/');
                    // filter -> membersihkan url (supaya url bersih dari karakter aneh)
                    $url = filter_var($url, FILTER_SANITIZE_URL);
                    // explode -> memecah url berdasarkan tanda slash (delimiter = slash, string = elemen array/url)
                    $url = explode('/', $url);
                    return $url;
                }
            }
        
        }
?>