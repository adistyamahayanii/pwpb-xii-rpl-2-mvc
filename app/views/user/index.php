<div class="container mt-5">

    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash(); ?>
        </div>
    </div>

   <div class="row">
       <div class="col-lg-6">
       <button type="button" class="btn btn-primary tombolTambahDataUser" data-bs-toggle="modal"data-bs-target="#formModal">
        Tambah Data User
        </button>
        <br></br>
           <h3>User</h3>
           <ul class="list-group">
           <?php foreach($data["user"] as $user) :?>
                <li class="list-group-item">
                    <?=$user['username']; ?>

                    <a href="<?=BASE_URL;?>/user/detail/<?= $user['id']?>"
                    class="btn btn-primary float-right">Detail</a>
                    
                    <a href="<?=BASE_URL;?>/user/ubah/<?= $user['id']?>"
                    class="btn btn-success float-right modalUbahUser" 
                    data-bs-toggle="modal" data-bs-target="#formModal"
                    data-id="<?= $user['id'];?>">Ubah</a>

                    <a href="<?=BASE_URL;?>/user/hapus/<?= $user['id']?>"
                    class="btn btn-danger float-right"
                    onclick="return confirm('Yakin Kak?');">Hapus</a>
                </li>
           <?php endforeach; ?>
           </ul>
       </div>
   </div>
</div>


<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabelUser">Tambah Data User</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

    <div class="modal-body">
        <form action="<?= BASE_URL; ?>/user/tambah" method="post">
        <input type="hidden" name="id" id="id">
            <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username">

                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
                </form>  
            </div>
        </div>
    </div>
</div>