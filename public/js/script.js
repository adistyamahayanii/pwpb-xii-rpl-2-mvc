$(function() {

    $('.tombolTambahData').on('click', function() {
        $('#formModalLabel').html('Tambah Data Blog');
        $('.modal-footer button[type=submit]').html('Tambah Data');
        $('#penulis').val('');
        $('#judul').val('');
        $('#tulisan').val('');
        $('#id').val('');
    });


    $('.tombolTambahDataUser').on('click', function() {
        $('#formModalLabelUser').html('Tambah Data User');
        $('.modal-footer button[type=submit]').html('Tambah Data');
        $('#username').val('');
        $('#email').val('');
        $('#password').val('');
        $('#id').val('');
    });
    
    $('.modalUbahUser').on('click', function() {
        
        $('#formModalLabelUser').html('Ubah Data User');
        $('.modal-footer button[type=submit]').html('Ubah Data');
        $('.modal-body form').attr('action', 'http://localhost/MVC-dasar/public/user/ubah');

        const id = $(this).data('id');
        
        $.ajax({
            url: 'http://localhost/MVC-dasar/public/user/getubah',
            data: {id : id},
            method: 'post',
            dataType: 'json',
            success: function(data) {
                console.log ('ok');
                $('#username').val(data.username);
                $('#email').val(data.email);
                $('#password').val(data.password);
                $('#id').val(data.id);
            }
        });

        
    });

    $('.tampilModalUbah').on('click', function() {
        
        $('#formModalLabel').html('Ubah Data Blog');
        $('.modal-footer button[type=submit]').html('Ubah Data');
        $('.modal-body form').attr('action', 'http://localhost/MVC-dasar/public/blog/ubah');

        const id = $(this).data('id');
        
        $.ajax({
            url: 'http://localhost/MVC-dasar/public/blog/getubah',
            data: {id : id},
            method: 'post',
            dataType: 'json',
            success: function(data) {
                $('#penulis').val(data.penulis);
                $('#judul').val(data.judul);
                $('#tulisan').val(data.tulisan);
                $('#id').val(data.id);
            }
        });

        
    });

});