<?php
   class Home extends Controller {
       public function index() {
           // echo "Home/index";
           $data["judul"] = "Home";
           $data["username"] = "Adistya";
           $this->view("templates/header", $data);
           $this->view("home/index", $data);
           $this->view("templates/footer");
       }
   }
?>