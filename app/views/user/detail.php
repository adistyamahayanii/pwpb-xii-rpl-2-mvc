<div class="container">
<div class="card" style="width: 18rem">
    <div class="card-body">
        <h5 class="card-title">Detail User</h5>
        <h6 class="card-subtitle mb-2 text-muted"><?= $data["user"]["username"];?></h6>
        <h6 class="card-subtitle mb-2 text-muted"><?= $data["user"]["email"];?></h6>
        <p class="card-text"><?= $data["user"]["password"];?></p>
        <a href="<?= BASE_URL?>/user/" class="card-link">Back</a>
    </div>
</div>
</div>
