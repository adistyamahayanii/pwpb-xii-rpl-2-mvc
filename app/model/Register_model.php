<?php
    class Register_Model 
    {
        private $table = 'user';
        private $db;

        public function __construct()
        {
            $this->db = new Database;
        }

        public function insertUser($data)
        {
            $query = "INSERT INTO user (username, email, password)
            VALUES
            (:username, :email, :password)";

            $this->db->query($query);
            $this->db->bind('username', $data['username']);
            $this->db->bind('email', $data['email']);
            $this->db->bind('password', $data['password']);

            $this->db->execute();

            return $this->db->rowCount();
        }

        public function check_user($username,$email)
        {
        $result= $this->db->select("SELECT * FROM user WHERE username = '".$username."' OR emailid = '".$email."'");
        $count = count($result);
        return $count;
        }


    }